package com.meixiaoliang.quartz;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class HelloJobTest {

    public static void main(String[] args) {

        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.putAsString("dog", 1);
        jobDataMap.put("cat", "cat");

        JobDetail jobDetail = JobBuilder.newJob(HelloJob.class).withIdentity("HelloJob").usingJobData(jobDataMap).build();

        System.out.println("name: " + jobDetail.getKey().getName());
        System.out.println("group: " + jobDetail.getKey().getGroup());
        System.out.println("jobClass: " + jobDetail.getJobClass().getName());

        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("DefaultTrigger").startNow()
            .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(3).repeatForever()).build();

        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            System.out.println("开始调度任务");
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

    }

}