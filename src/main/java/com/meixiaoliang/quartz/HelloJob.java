package com.meixiaoliang.quartz;

import java.time.LocalDateTime;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJob implements Job {

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("quartz job, time: " + LocalDateTime.now());
        System.out.println(jobExecutionContext.getJobDetail().getJobDataMap().get("cat"));
        System.out.println(jobExecutionContext.getJobDetail().getJobDataMap().get("dog"));
    }

}